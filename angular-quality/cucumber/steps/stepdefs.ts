import { expect } from 'chai';
import { Before, Given, Then, When } from 'cucumber';
import { AppPage } from './pages/app.po';


let page: AppPage;

Before(() => {
  page = new AppPage();
});

Given('I am on the home page', async () => {
  await page.navigateTo();
});

Given('I am {string} with ID {string}', async (string, string2) => {
  // Write code here that turns the phrase above into concrete actions
  await page.fillNameInput(string);
  await page.fillCustomerNumberInput(string2);
});

When('I click submit', async () => {
  // Write code here that turns the phrase above into concrete actions
  await page.getSubmitButton().click();
});

Then('I should see Premium-Offers', async () => {
  // Write code here that turns the phrase above into concrete actions
  expect(await page.getPremimOffers().getText()).to.contain('Premiumangebote');
});

Then('I should not see Premium-Offers', async () => {
  // Write code here that turns the phrase above into concrete actions
  // tslint:disable-next-line
  expect(await page.getPremimOffers().isPresent()).to.be.false;
});
