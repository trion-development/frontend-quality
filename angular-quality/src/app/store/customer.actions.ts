import { Action } from '@ngrx/store';
import { Customer } from './customer.reducer';

export enum CustomerActionTypes {
  SetCustomer = '[Customer] Set Customer'
}

export class SetCustomer implements Action {
  readonly type = CustomerActionTypes.SetCustomer;

  constructor(public customer: Customer) {
  }
}

export type CustomerActions = SetCustomer;
