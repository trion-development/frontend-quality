import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'angular-quality';

  readonly COMMIT_HASH = (window && window['commit_hash']) || '';
  readonly BUILD_DATE = (window && window['build_date']) || '';
}
